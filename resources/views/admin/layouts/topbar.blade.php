<nav class="main-header navbar navbar-expand-md navbar-dark navbar-cyan">
    <div class="container">
        <a href="{{ route('dashbaord.index') }}" class="navbar-brand">
        Appointment System
        <!-- <span class="brand-text font-weight-light"><b>YUZUH</b></span> -->
        </a>
        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item @if(Request::segment(2) == 'dashboard') active @endif">
                    <a href="{{ route('dashbaord.index') }}" class="nav-link">Dashboard</a>
                </li>

                <li class="nav-item @if(Request::segment(2) == 'dashboard') active @endif">
                    <a href="{{ route('appointment.index') }}" class="nav-link">Appointment</a>
                </li>
                
    
                
            
        </div>
        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user"></i>&nbsp;
                <?= Auth::guard('web')->user()->name; ?>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item">
                        <i class="fa fa-key mr-2"></i>Logout
                    </a>
                </div>
            </li>
        </ul>
        
    </div>
</nav>
