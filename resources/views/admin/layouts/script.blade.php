<!-- jquery -->
<?= Html::script('admin/js/jquery.min.js', [], IS_SECURE) ?>

<!-- Bootstrap 4 -->
<?= Html::script('admin/js/bootstrap.bundle.min.js', [], IS_SECURE) ?>

<!-- AdminLTE App -->
<?= Html::script('admin/js/adminlte.min.js', [], IS_SECURE) ?>



<!-- AdminLTE for demo purposes -->
<?= Html::script('admin/js/demo.js', [], IS_SECURE) ?>
<!-- toastr -->
<?= Html::script('admin/js/toastr.min.js', [], IS_SECURE) ?>

<?= Html::script('admin/js/bootstrap-fileupload.js', [], IS_SECURE) ?>

<?= Html::script('admin/js/jquery.form.min.js', [], IS_SECURE) ?>

<?= Html::script('admin/js/select2.min.js', [], IS_SECURE) ?>

<?= Html::script('admin/plugins/sweetalert2/sweetalert2.all.js', [], IS_SECURE) ?>

<?= Html::script('admin/plugins/bootstrap-fileinput-master/js/fileinput.min.js', [], IS_SECURE) ?>
<?= Html::script('admin/plugins/bootstrap-fileinput-master/js/locales/es.js', [], IS_SECURE) ?>
<?= Html::script('admin/plugins/bootstrap-fileinput-master/js/plugins/piexif.min.js', [], IS_SECURE) ?>
<?= Html::script('admin/plugins/bootstrap-fileinput-master/js/plugins/sortable.min.js', [], IS_SECURE) ?>
<?= Html::script('admin/plugins/bootstrap-fileinput-master/themes/fas/theme.js', [], IS_SECURE) ?>



