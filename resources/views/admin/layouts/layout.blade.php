<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>Appointment System | Admin</title>
        @include('admin.layouts.style')
        @yield('style')
        
    </head>
    <body class="hold-transition layout-top-nav">
        <div class="wrapper">
            <!-- Navbar -->
            @include('admin.layouts.topbar')
            <!-- /.navbar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="font-size:14.5px;">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            @include('admin.layouts.footer')
            <!-- /.main footer -->
        </div>
        
        @include('admin.layouts.script')
        @yield('script')
    </body>
</html>
