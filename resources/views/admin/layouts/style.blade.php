
<?= Html::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"',[],IS_SECURE) ?>

<!-- Font Awesome Icons -->
<?= Html::style('admin/css/fontawesome-free/css/all.min.css',[],IS_SECURE) ?>

<!-- Theme style -->
<?= Html::style('admin/css/adminlte.min.css',[],IS_SECURE) ?>
<!-- icheck bootstrap -->
<?= Html::style('admin/css/icheck-bootstrap.min.css',[],IS_SECURE) ?>

<?= Html::style('admin/css/toastr.min.css',[],IS_SECURE) ?>
<!-- bootstrap fileupload -->
<?= Html::style('admin/css/bootstrap-fileupload.css',[],IS_SECURE) ?>

<?= Html::style('admin/css/style.css',[],IS_SECURE) ?>

<?= Html::style('admin/css/select2.min.css',[],IS_SECURE) ?>

<?= Html::style('admin/plugins/sweetalert2/sweetalert2.min.css',[],IS_SECURE) ?>
