@extends('admin.layouts.layout')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0"> </h1>
            </div>
            
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>

<section class="content">
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-8">
                
                <div class="card card-primary">
                     <div class="card-body ">
                <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
              <!-- /.card-body -->
                </div>
            <!-- /.card -->
          </div>
           
            
            
        </div>
    </div>
</section>


       
@stop
@section('style')
<?= Html::style('admin/plugins/fullcalendar/main.css', [], IS_SECURE) ?>
<?= Html::style('admin/plugins/daterangepicker/daterangepicker.css', [], IS_SECURE) ?>

@stop
@section('script')
<?= Html::script('admin/plugins/moment/moment.min.js', [], IS_SECURE) ?>
<?= Html::script('admin/plugins/fullcalendar/main.js', [], IS_SECURE) ?>
<?= Html::script('admin/plugins/daterangepicker/daterangepicker.js', [], IS_SECURE) ?>

    <script>
  $(function () {

   
    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

        console.log(m);

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendar.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');

    // initialize the external events
    // -----------------------------------------------------------------


    var calendar = new Calendar(calendarEl, {
      headerToolbar: {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      themeSystem: 'bootstrap',
      //Random default events
      events: [
        @foreach($all_data as $key => $value)
        
        {
          title          : '<?= $value['title'] ?>',
          start          : '<?= $value['start_time'] ?>',
          end          :    '<?= $value['end_time'] ?>',
          backgroundColor: '#f56954', //red
          borderColor    : '#f56954', //red
          allDay         : true
        },
        @endforeach
        
      ],
      
      
    });

    calendar.on('dateClick', function(info) {
      window.location.href = '<?= route('appointment.index'); ?>'
    });

    calendar.render();
    
  })
</script>
@stop