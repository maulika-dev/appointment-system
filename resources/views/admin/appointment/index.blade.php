@extends('admin.layouts.layout')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Appointment Manager</h1>
            </div>
            <div class="col-sm-6 text-right">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('appointment.create') }}" class="btn btn-primary" title="Add New"><i class="fa fa-plus-circle"></i> Add New</a>&nbsp;
                    <a href="javascript:;" class="btn btn-danger delete_record" title="Delete"><i class="fas fa-times-circle"></i> Delete</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                      <table id="master_table" class="table table-bordered table-striped" style="width: 100%">
                        <thead>
                            <tr>
                                <th class="select-all no-sort">
                                    <div class="animated-checkbox">
                                        <label class="m-0">
                                            <input id="checkAll" class="select_check_box" type="checkbox"/>
                                            <span class="label-text">
                                            </span>
                                        </label>
                                    </div>
                                </th>
                                <th>Title</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.appointment.show');
@stop
@section('style')
{{ Html::style('admin/plugins/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css') }}
{{ Html::style('admin/plugins/datatables/datatables-responsive/css/responsive.bootstrap4.min.css') }}

@stop

@section('script')
{{ Html::script('admin/plugins/datatables/datatables/jquery.dataTables.min.js') }}
{{ Html::script('admin/plugins/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js') }}
{{ Html::script('admin/plugins/datatables/datatables-responsive/js/dataTables.responsive.min.js') }}
{{ Html::script('admin/plugins/datatables/datatables-responsive/js/responsive.bootstrap4.min.js') }}
{{ Html::script('admin/js/delete_script.js') }}
{{ Html::script('admin/js/fnStandingRedraw.js') }}



<script type="text/javascript">
    var title = "Are you sure to delete selected record(s)?";
    var text = "You will not be able to recover this record";
    var type = "warning";
    var delete_path = "{{ URL::route('appointment.delete') }}";
    var token = "{{ csrf_token() }}";
	
    $(function () {        
        $('.delete_record').click(function(){
            var delete_id = $('#master_table tbody input[type=checkbox]:checked');
            console.log(delete_id);
            checkLength(delete_id);
        });

        $(".select_check_box").on('click', function(){
            var is_checked = $(this).is(':checked');
            $(this).closest('table').find('tbody tr td:first-child input[type=checkbox]').prop('checked',is_checked);
            $(".select_check_box").prop('checked',is_checked);
        });

        var t = $('#master_table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching":true,
            "processing": true,
            "serverSide": true,
            "sAjaxSource": "{{URL::route('appointment.index')}}",
            "aaSorting": [
                [2, "desc"]
            ],
    
            "aoColumns":[
                {   
                    mData: "id",
                    bSortable:false,
                    sWidth:"2%",
                    sClass:"text-center",
                    mRender: function (v, t, o) {
                        return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id['+v+']" value="'+v+'"/><span class="label-text"></span></label></div>';
                    },
                },

                
                { mData:"title",sClass : 'text-left',bSortable:true ,sWidth:"15%",bVisible:true
                },

                { mData:"start_time",sClass : 'text-left',bSortable:true ,sWidth:"10%",bVisible:true
                },

                { mData:"end_time",sClass : 'text-left',bSortable:true ,sWidth:"10%",bVisible:true
                    // mRender: function(v,t,o){
                    //     var category_detail = o['get_category'];
                    //     var data = "";
                    //     if(category_detail != null){
                    //         data = category_detail['name'];
                    //     }
                    //     return data;
                    // }
                },

                {
                    mData: null,
                    bSortable: false,
                    sWidth: "3%",
                    sClass: "text-center",
                    mRender: function(v, t, o) {

                        var editurl = "{{ route('appointment.edit',':id')}}";
                        editurl = editurl.replace(':id',o['id']);

                        var showurl = "{{ route('appointment.show',':id')}}";
                        showurl = showurl.replace(':id',o['id']);

                        var act_html = "<div class='btn-group'>"
                        +"<a href='"+editurl+"'class='btn btn-default btn-sm' title='Edit appointments'><i class='fa fa-pencil-square-o'></i>Edit</a>"

                        +"<a href='javascript:void(0)' onclick= \"detailModal("+o['id']+")\" class='btn btn-default btn-sm' title='Edit appointments'><i class='fa fa-pencil-square-o'></i>Show</a>"

                        +"<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\"class='btn btn-default btn-sm' title='Delete appointments'> Delete</a>"
                        +"</div>"
                        return act_html;
                    }
                },
            ],
            fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
            fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
            },
        });

    });

    function detailModal(id)
    {
        var token = "{{ csrf_token() }}";
        $.ajax({
            url: "<?=route('appointment.show')?>",
            type: 'get',
            data: { "_token": token,'id':id },
            dataType: 'json',
            success: function (resp) {
                var data = resp.data;
                $('#detailModal').modal('show');
                $('#modalBody').html(resp.html);
            },
        });
    }
</script>
@include('admin.layouts.alert')
@stop