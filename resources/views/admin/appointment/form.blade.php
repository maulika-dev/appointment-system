<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title m-0">Edit Appointment</h5>
                    </div>
                   
                    <div class="card-body">
                        
                        <div class="form-group row">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('title','',['class'=>'col-form-label'])?><span class="text-danger"> *</span>
                            </div>
                            <div class="col-sm-8">
                                <?= Form::text('title',old('title'),['class'=>'form-control','id'=>'title','placeholder'=>'Enter title'])?>
                                <span class='text-danger' id="title_error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('start_time','',['class'=>'col-form-label'])?><span class="text-danger"> *</span>
                            </div>
                            <div class="col-sm-8">
                                <?= Form::text('start_time',old('start_time'),['class'=>'form-control','id'=>'start_time','placeholder'=>'Enter Start Time'])?>
                                <span class='text-danger' id="start_time_error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('end time','',['class'=>'col-form-label'])?><span class="text-danger"> *</span>
                            </div>
                            <div class="col-sm-8">
                                <?= Form::text('end_time',old('end_time'),['class'=>'form-control','id'=>'end_time','placeholder'=>'Enter end time'])?>
                                <span class='text-danger' id="end_time_error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('notes','',['class'=>'col-form-label'])?>
                            </div>
                            <div class="col-sm-8">
                                <?= Form::textarea('notes',old('notes'),['class'=>'form-control','id'=>'notes','placeholder'=>'Enter notes','rows'=>4])?>
                                <span class='text-danger' id="notes_error"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('files','',['class'=>'control-label'])?>
                            </div>
                            <div class="form-group col-sm-8">
                               <?=Form::file('files', ['class' => 'control-label'])?>
                               @if(isset($data['files']))
                               <a href="<?= UPLOAD_URL.'/document/'.$data['files'] ?>"><?= $data['files'] ?></a>
                               @endif
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
                            <div class="text-right">
                                @if(Request::segment(3) == 'create')
                                <button type="button" id="save_new" class="btn btn-primary btn-sm save" title="Save & New">Save & New</button>
                                @endif

                                <button type="button" id="save_exit" class="btn btn-primary btn-sm save" title="Save & Exit">Save & Exit</button>
                                    
                                <a href="{{ route('appointment.index') }}" title='Back'><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>