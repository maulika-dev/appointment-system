<div class="row">
	<div class="col-md-12 table-responsive">
		<table class="table table-bordered table-striped">

			<tbody>
				
				<tr>
					<td>Notes</td>
					<td>{{$data['notes']}}</td>
				</tr>
				
				<tr>
					<td>Attachment</td>
					<td><a href="<?= UPLOAD_URL.'/document/'.$data['files'] ?> " _blank>Attachment Link</a></td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>

