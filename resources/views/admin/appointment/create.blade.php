@extends('admin.layouts.layout')
@section('content')
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0"> Appointment Manager </h1>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@include('admin.layouts.overlay')
<?= Form::open(['id'=>'save_form']) ?>
    @include('admin.appointment.form',['title'=>'Add appointment'])
<?= Form::close(); ?>
@stop

@section('style')

<?= Html::style('admin/plugins/daterangepicker/daterangepicker.css', [], IS_SECURE) ?>

@stop
@section('script')

<?= Html::script('admin/plugins/daterangepicker/daterangepicker.js', [], IS_SECURE) ?>



    <script type="text/javascript">
        $(".save").click(function(e){
            e.preventDefault();

            var btn_name = $(this).attr('title');
            var val = $(this).val();
            var url = "<?= URL::route('appointment.store') ?>";
            var method_type = 'post';
            var token = "<?=csrf_token()?>";

            $('#save_form').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : token },
                dataType: 'json',
                beforeSubmit : function()
                {
                    $('#loader').show();
                    if(btn_name == 'Save & New')
                    {
                        $('#save_new').attr('disabled',true);
                        $('#save_new').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    else
                    {
                        $('#save_exit').attr('disabled',true);
                        $('#save_exit').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    $("[id$='_error']").empty();
                },
                success : function(resp)
                {                   
                    if (resp.success == true) {
                        var action = resp.action;
                        toastr.success('appointment Successfully Added');

                        if(btn_name == 'Save & New')
                        {
                            $('#save_new').attr('disabled',false);
                            $('#save_new').html('Save & New');
                            window.location.href = "{{route('appointment.create')}}";
                        }
                        else
                        {
                            $('#save_exit').attr('disabled',false);
                            $('#save_exit').html('Save & Exit');
                            window.location.href = "{{route('appointment.index')}}";
                        }
                    }              
                },
                error : function(respObj){    

                    toastr.error('Something Went Wrong');
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    // toastr.error('there were some errors!');
                    if(btn_name == 'Save & New')
                    {
                        $('#save_new').attr('disabled',false);
                        $('#save_new').html('Save & New');
                    }
                    else
                    {
                        $('#save_exit').attr('disabled',false);
                        $('#save_exit').html('Save & Close');
                    }
                    $("#loader").hide();
                }
            });
        });

        $('#start_time').datetimepicker({
            format:'Y-m-d H:i',
            formatTime:'H:i',

        });
        $('#end_time').datetimepicker({
            format:'Y-m-d H:i',
            formatTime:'H:i',
        });
    </script>
@stop
@include('admin.layouts.alert')
