<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel = "icon" href = "<?= FEVOCON_ICON ?>"> 
        <title>Yuzuh | Distributor</title>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
        @include('admin.layouts.style')
        @yield('style')
        
    </head>
<body class="hold-transition layout-top-nav">
    <div class="wrapper" id="app">
        @include('distributor.layouts.topbar')
        <h3>Welcome to Laravel World!!!</h3>
        <app></app>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>