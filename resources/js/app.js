require('./bootstrap');

import Vue from 'vue'
import axios from "axios";
import VueResource from 'vue-resource';
import VueObserveVisibility from 'vue-observe-visibility'

//Main pages
import App from './component/example.vue'
import Productlist from './component/ProductList.vue';


Vue.use(VueResource);
Vue.use(VueObserveVisibility)

const app = new Vue({
    el: '#app',
    components: { Productlist }
});

