<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Auth::routes();

Route::namespace('App\Http\Controllers')->middleware(['auth','web'])->group( function(){

	Route::get('/logout','Auth\LoginController@logout')->name('admin.logout');

	Route::get('/appointment','AppointmentController@index')->name('appointment.index');

	Route::get('/appointment/create','AppointmentController@create')->name('appointment.create');

	Route::post('/appointment/store','AppointmentController@store')->name('appointment.store');

	Route::get('/appointment/{id}/edit','AppointmentController@edit')->name('appointment.edit');

	Route::post('/appointment/{id}/update','AppointmentController@update')->name('appointment.update');

	Route::post('/appointment/delete','AppointmentController@delete')->name('appointment.delete');

	Route::get('/dashboard','DashboardController@index')->name('dashbaord.index');

	Route::get('/appointment/show','AppointmentController@show')->name('appointment.show');
});


