<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\ImageUploadTrait;
use App\Models\SectionSetting;
use App\Models\HomePartsDetails;
use App\Models\HomeStepsDetail;
use App\Models\HomeIconDetail;
use App\Models\HomeFeaturesDetail;
use Auth;

class SectionSettingJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($section_data)
    {
        $this->section_data = $section_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $section_data = $this->section_data;
        /// dd($section_data);

        $id = '';
        
        if (isset($section_data['id'])) {
            $id = $section_data['id'];
        }

        $save_data = SectionSetting::firstOrNew(['id'=>$id]);

        // dd($section_data);
        // $save_data->fill($section_data);

        // $save_data->banner_title = $section_data['banner_title'];
        // $save_data->banner_description = $section_data['banner_description'];

        // if (isset($section_data['banner_image'])) {

        //     $old_image = $save_data->banner_image;
        //     $foldername = 'home-page';
        //     $image_data = $section_data['banner_image'];
        //     $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
        //     // dd($filename);
        //     $save_data->banner_image = $filename;
        // }

        // dd($save_data);

        $save_data->about_us_description = $section_data['about_us_description'];
        $save_data->business_description = $section_data['business_description'];
        $save_data->whyus_description = $section_data['whyus_description'];
        
        $save_data->values_title = $section_data['values_title'];
        $save_data->values_description = $section_data['values_description'];
        if (isset($section_data['values_image'])) {

            $old_image = $save_data->values_image;
            $foldername = 'home-page';
            $image_data = $section_data['values_image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->values_image = $filename;
        }

        $save_data->whyus_title = $section_data['whyus_title'];
        $save_data->whyus_btn_name = $section_data['whyus_btn_name'];
        $save_data->whyus_btn_link = $section_data['whyus_btn_link'];
        $save_data->whyus_short_description = $section_data['whyus_short_description'];

        $save_data->values_box1_title = $section_data['values_box1_title'];
        $save_data->values_box1_description = $section_data['values_box1_description'];
        if (isset($section_data['values_box1_image'])) {

            $old_image = $save_data->values_box1_image;
            $foldername = 'home-page';
            $image_data = $section_data['values_box1_image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->values_box1_image = $filename;
        }

        $save_data->values_box2_title = $section_data['values_box2_title'];
        $save_data->values_box2_description = $section_data['values_box2_description'];
        if (isset($section_data['values_box2_image'])) {

            $old_image = $save_data->values_box2_image;
            $foldername = 'home-page';
            $image_data = $section_data['values_box2_image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->values_box2_image = $filename;
        }

        $save_data->values_box3_title = $section_data['values_box3_title'];
        $save_data->values_box3_description = $section_data['values_box3_description'];
        if (isset($section_data['values_box3_image'])) {

            $old_image = $save_data->values_box3_image;
            $foldername = 'home-page';
            $image_data = $section_data['values_box3_image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->values_box3_image = $filename;
        }

        
        $save_data->save();

        // features details
        if (isset($section_data['features']) && !empty($section_data['features'])) {
            $features_delete = HomeFeaturesDetail::where('section_setting_id',$save_data->id)->delete();
            foreach ($section_data['features'] as $key => $value) {
                $features_data = new HomeFeaturesDetail();
                $features_data->text = $value;
                $features_data->section_setting_id = $save_data->id;
                $features_data->updated_by = Auth::user()->id;
                $features_data->save();
            }
        }

        // save part details
        if (!empty($section_data['part_title']) && !empty($section_data['part_description']) && !empty($section_data['part_icon'])) 
        {
            for($i=0;$i<3;$i++)
            {
                $part_data = HomePartsDetails::firstOrNew(['section_setting_id'=>$id,'id'=>$i+1]);
                $part_data->title = $section_data['part_title'][$i];
                $part_data->description = $section_data['part_description'][$i];
                $part_data->icon_image = $section_data['part_icon'][$i];
                $part_data->section_setting_id = $save_data->id;
                $part_data->updated_by = Auth::user()->id;
                $part_data->save();
            }
        }
        
        // save icon details
        if (!empty($section_data['icon_title']) && !empty($section_data['icon_subtitle']) && !empty($section_data['icon_image']) ) {
            for($i=0;$i<2;$i++)
            {
                $part_data = HomeIconDetail::firstOrNew(['section_setting_id'=>$id,'id'=>$i+1]);
                $part_data->title = $section_data['icon_title'][$i];
                $part_data->sub_title = $section_data['icon_subtitle'][$i];
                $part_data->icon_image = $section_data['icon_image'][$i];
                $part_data->section_setting_id = $save_data->id;
                $part_data->updated_by = Auth::user()->id;
                $part_data->save();
            }
        }
    }
}
