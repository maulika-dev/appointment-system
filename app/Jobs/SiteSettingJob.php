<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\ImageUploadTrait;
use App\Models\SiteSetting;
use Auth;

class SiteSettingJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($site_data)
    {
        $this->site_data = $site_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $site_data = $this->site_data;
        // dd($site_data);

        $id = '';
        
        if (isset($site_data['id'])) {
            $id = $site_data['id'];
        }

        $save_data = SiteSetting::firstOrNew(['id'=>$id]);

        if (isset($site_data['primary_logo']) && !empty($site_data['primary_logo'])) {
            // dd('primary');
            $old_image = $save_data->primary_logo;
            $foldername = 'logo';
            $image_data = $site_data['primary_logo'];
            $primary_filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->primary_logo = $primary_filename;
            unset($site_data['primary_logo']);
        }        
        else
        {
            unset($site_data['primary_logo']);
        }
        if (isset($site_data['secondary_logo']) && !empty($site_data['secondary_logo'])) {
            // dd('secondary');
            $old_image = $save_data->secondary_logo;
            $foldername = 'logo';
            $image_data = $site_data['secondary_logo'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->secondary_logo = $filename;
            unset($site_data['secondary_logo']);
        }
        else
        {
            unset($site_data['secondary_logo']);
        }

        if (isset($site_data['favicon_icon']) && !empty($site_data['favicon_icon']) ) {
            // dd('fevicon');
            $old_image = $save_data->favicon_icon;
            $foldername = 'fevicon';
            $image_data = $site_data['favicon_icon'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->favicon_icon = $filename;
            unset($site_data['favicon_icon']);
        }
        else
        {
            unset($site_data['favicon_icon']);
        }
        $save_data->fill($site_data);

        $save_data->company_name = $site_data['company_name'];
        $save_data->seo_title = $site_data['seo_title'];
        $save_data->seo_description = $site_data['seo_description'];
        $save_data->contact_email = $site_data['contact_email'];
        $save_data->contact_no_1 = $site_data['contact_no_1'];
        $save_data->contact_no_2 = $site_data['contact_no_2'];
        $save_data->address = $site_data['address'];
        $save_data->google_map_iframe = $site_data['google_map_iframe'];
        $save_data->google_analytics_code = $site_data['google_analytics_code'];
        $save_data->facebook_url = $site_data['facebook_url'];
        $save_data->twitter_url = $site_data['twitter_url'];
        $save_data->instagram_url = $site_data['instagram_url'];
        
        $save_data->save();
        // dd($save_data);
    }
}
