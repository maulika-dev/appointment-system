<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Customer;
use Auth;

class CustomerJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($customer_data)
    {
        $this->customer_data = $customer_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $customer_data  = $this->customer_data;
        $id = '';
        
        if (isset($customer_data['id'])) {
            $id =$customer_data['id'];
        }

        $save_data = Customer::firstOrNew(['id'=>$id]);
        $save_data->fill($customer_data);

        $save_data->full_name = $customer_data['full_name'];
        $save_data->email = $customer_data['email'];
        $save_data->contact_no = $customer_data['contact_no'];
        $save_data->address = $customer_data['address'];
        $save_data->state_id = $customer_data['state'];
        $save_data->city_id = $customer_data['city'];
        $save_data->gst_no = $customer_data['gst_no'];
        
        if ($customer_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
