<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Manufacturer;
use Auth;

class ManufacturerJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($manufacturer_data)
    {
        $this->manufacturer_data = $manufacturer_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $manufacturer_data  = $this->manufacturer_data;
        $id = '';
        
        if (isset($manufacturer_data['id'])) {
            $id =$manufacturer_data['id'];
        }
        // dd($manufacturer_data);
        $save_data = Manufacturer::firstOrNew(['id'=>$id]);
        $save_data->fill($manufacturer_data);

        $save_data->name = $manufacturer_data['name'];
        $save_data->address = $manufacturer_data['address'];
        $save_data->state_id = $manufacturer_data['state'];
        $save_data->city_id = $manufacturer_data['city'];
        $save_data->poc_name = $manufacturer_data['poc_name'];
        $save_data->poc_email = $manufacturer_data['poc_email'];
        $save_data->poc_phone_1 = $manufacturer_data['poc_phone_1'];
        $save_data->poc_phone_2 = $manufacturer_data['poc_phone_2'];
        $save_data->poc_description = $manufacturer_data['poc_description'];
        
        if ($manufacturer_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        $save_data->save();
    }
}
