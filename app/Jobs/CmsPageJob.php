<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Auth;
use App\Models\CmsPage;
use Illuminate\Support\Str;
class CmsPageJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($page_data)
    {
        $this->page_data = $page_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page_data = $this->page_data;

        $id = '';
        
        if (isset($page_data['id'])) {
            $id =$page_data['id'];
        }

        $save_data = CmsPage::firstOrNew(['id'=>$id]);
        $save_data->fill($page_data);
        // dd($save_data);
        $save_data->page_title = $page_data['page_title'];
        if(!empty($page_data['slug']))
        {
            $save_data->slug = $page_data['slug'];
        }
        else
        {
            $save_data->slug = Str::slug($page_data['page_title'],'-');  
        }
        
        $save_data->meta_title = $page_data['meta_title'];
        $save_data->meta_description = $page_data['meta_description'];
        $save_data->page_content = $page_data['page_content'];
       
        if ($page_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }
        $save_data->save();
        // dd($save_data);
    }
}
