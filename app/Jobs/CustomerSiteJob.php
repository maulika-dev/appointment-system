<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\CustomerSite;
use Auth;

class CustomerSiteJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($site_data)
    {
        $this->site_data = $site_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $site_data  = $this->site_data;
        $id = '';
        
        if (isset($site_data['id'])) {
            $id =$site_data['id'];
        }

        $save_data = CustomerSite::firstOrNew(['id'=>$id]);
        $save_data->fill($site_data);

        $save_data->site_alias = $site_data['site_alias'];
        $save_data->customer_id = $site_data['customer'];
        $save_data->address = $site_data['address'];
        $save_data->state_id = $site_data['state'];
        $save_data->city_id = $site_data['city'];
        
        if ($site_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
