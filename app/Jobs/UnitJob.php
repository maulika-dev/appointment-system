<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Unit;
use Auth;

class UnitJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($unit_data)
    {
        $this->unit_data = $unit_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $unit_data  = $this->unit_data;
        $id = '';
        
        if (isset($unit_data['id'])) {
            $id =$unit_data['id'];
        }

        $save_data = Unit::firstOrNew(['id'=>$id]);
        $save_data->fill($unit_data);

        $save_data->name = $unit_data['name'];
        $save_data->unit_code = $unit_data['unit_code'];
        $save_data->status = $unit_data['status'];
        
        if ($unit_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
