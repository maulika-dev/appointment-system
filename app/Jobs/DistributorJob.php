<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Distributor;
use Illuminate\Support\Str;
use Auth;
use App\Http\Traits\HelperTrait;
class DistributorJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,HelperTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($distributor_data)
    {
        $this->distributor_data = $distributor_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $distributor_data  = $this->distributor_data;
        $id = '';
        
        if (isset($distributor_data['id'])) {
            $id =$distributor_data['id'];
        }

        $save_data = Distributor::firstOrNew(['id'=>$id]);
        $save_data->fill($distributor_data);

        $save_data->name = $distributor_data['name'];
        $save_data->company_name = $distributor_data['company_name'];
        $save_data->company_type = $distributor_data['company_type'];
        if (isset($distributor_data['email'])) {
            $save_data->email = $distributor_data['email'];
        }
        $raw_password = Str::random(8);
        $save_data->password = bcrypt($raw_password);
        $save_data->address = $distributor_data['address'];
        $save_data->area = $distributor_data['area'];
        $save_data->state_id = $distributor_data['state'];
        $save_data->city_id = $distributor_data['city'];
        $save_data->phone_1 = $distributor_data['phone_1'];
        $save_data->phone_2 = $distributor_data['phone_2'];
        $save_data->poc_name = $distributor_data['poc_name'];
        $save_data->poc_email = $distributor_data['poc_email'];
        $save_data->poc_phone = $distributor_data['poc_phone'];
        $save_data->poc_description = $distributor_data['poc_description'];
        $save_data->margin_total_amount = '0';
        $save_data->status = $distributor_data['status'];

        if ($distributor_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;

            // send mail to distributor

            $email_data = ['name'=>$distributor_data['name'],'email'=>$distributor_data['email'],'password'=>$raw_password]; 

            $email_view = 'admin.emails.distributor_email';
            $subject = 'Welcome';
            $email = $distributor_data['email'];
            $this->SendMail($email_view,$subject,$email_data,$email);
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        

        // dd($save_data);
        $save_data->save();
    }
}
