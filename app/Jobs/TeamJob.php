<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Team;
use App\Http\Traits\ImageUploadTrait;
use Auth;

class TeamJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($team_data)
    {
        $this->team_data = $team_data;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $team_data = $this->team_data;

        $id = '';
        
        if (isset($team_data['id'])) {
            $id =$team_data['id'];
        }

        $save_data = Team::firstOrNew(['id'=>$id]);
    
        $save_data->name = $team_data['name'];
        $save_data->designation = $team_data['designation'];
        $save_data->description = $team_data['description'];
        if (isset($team_data['image'])) {

            $old_image = $save_data->image_url;
            $foldername = 'home-page/team';
            $image_data = $team_data['image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->image_url = $filename;
        }
        
        if ($team_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
