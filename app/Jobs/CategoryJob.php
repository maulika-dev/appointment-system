<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\ImageUploadTrait;
use App\Models\Category;
use Auth;

class CategoryJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($category_data)
    {
        $this->category_data = $category_data;
    }

    /**
     * Execute the job.s
     *
     * @return void
     */
    public function handle()
    {
        $category_data  = $this->category_data;
        $id = '';
        
        if (isset($category_data['id'])) {
            $id =$category_data['id'];
        }

        $save_data = Category::firstOrNew(['id'=>$id]);
        $save_data->fill($category_data);

        $save_data->name = $category_data['name'];
        $save_data->code = $category_data['code'];
        $save_data->status = $category_data['status'];
        if (isset($category_data['image'])) {

            $old_image = $save_data->image_url;
            $foldername = 'category';
            $image_data = $category_data['image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->image_url = $filename;
        }
        
        if ($category_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
        
    }
}
