<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ManufaturerProduct;
use App\Http\Traits\ImageUploadTrait;
use Auth;

class ProductJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product_data)
    {
        $this->product_data = $product_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product_data  = $this->product_data;
        // dd($product_data);
        $id = '';
        
        if (isset($product_data['id'])) {
            $id =$product_data['id'];
        }

        $save_data = Product::firstOrNew(['id'=>$id]);
        // $save_data->fill($product_data);

        $save_data->name = $product_data['name'];
        $save_data->description = $product_data['description'];
        $save_data->category_id = $product_data['category'];
        $save_data->brand_id = $product_data['brand'];
        $save_data->unit = $product_data['unit'];
        $save_data->unit_price = $product_data['unit_price'];
        $save_data->unit_price = $product_data['unit_price'];
        $save_data->minimum_ord_qty = $product_data['minimum_ord_qty'];
        $save_data->credit_days = $product_data['credit_days'];
        $save_data->tax_percentage = $product_data['tax_percentage'];
        $save_data->hsn_code = $product_data['hsn_code'];
        $save_data->transportation_charges = $product_data['transportation_charges'];
        // $save_data->days_of_payment = $product_data['days_of_payment'];
        $save_data->days_0 = $product_data['days_0'];
        $save_data->days_1_15 = $product_data['days_1_15'];
        $save_data->days_16_30 = $product_data['days_16_30'];
        $save_data->days_31_45 = $product_data['days_31_45'];
        $save_data->days_46_60 = $product_data['days_46_60'];
        $save_data->days_61_75 = $product_data['days_61_75'];
        $save_data->days_76_90 = $product_data['days_76_90'];
        $save_data->days_91_120 = $product_data['days_91_120'];

        if ($product_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;

            $product_image_delete = ProductImage::where('product_id',$save_data->id)->delete();

            $manufacturer_product_delete = ManufaturerProduct::where('product_id',$save_data->id)->delete();
        }
        // dd($save_data);
        $save_data->save();

        $file_array = [];

        if (isset($product_data['manufacturer']) && !empty($product_data['manufacturer'])) {
            
            foreach ($product_data['manufacturer'] as $manu_key => $manu_value) {
                $manufacturer_create = new ManufaturerProduct();
                $manufacturer_create->manufacturer_id = $manu_value;
                $manufacturer_create->product_id = $save_data->id;
                $manufacturer_create->save();
            }
        }

        if (isset($product_data['removed_image']) && !empty($product_data['removed_image']))
        {
            $removed_image = explode(',',$product_data['removed_image']);
            // dd($removed_image);
            foreach ($removed_image as $key => $value) {
                
                $path = UPLOAD_PATH."/product/".$value;
                if(file_exists($path))
                {
                    unlink($path);
                }
            }
        }

        if (isset($product_data['remaining_image']) && !empty($product_data['remaining_image'])) {

            $remaining_image = explode(',',$product_data['remaining_image']);
            foreach ($remaining_image as $key => $value) {
                array_push($file_array,$value);
            }
            
            // dd($file_array);
        }
        
        if (isset($product_data['files'])) {
            if (!empty($product_data['files'])) {
                $old_image = $save_data->image_url;
                foreach ($product_data['files'] as $files_key => $files_value) {
                    // dd($files_value);
                    $foldername = 'product/';
                    $image_data = $files_value;
                    $filename = $this->MultipleImageUpload($image_data,$foldername);
                    // dd($filename);
                    array_push($file_array,$filename);
                    // add images in product images table
                }
            }
            
        }

        if (!empty($file_array)) {
            
            foreach ($file_array as $key => $value) {
                
                $product_image = new ProductImage();
                $product_image->product_id = $save_data->id;
                $product_image->image_name = $value;
                $product_image->save();
            }
        }

        // $update_image_data = Product::where('id',$save_data->id)
        //                                     ->update(['image_url'=>$file_array]);
        
    }
}
