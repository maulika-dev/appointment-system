<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TaxSlab;
use Auth;

class TaxSlabJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tax_slab_data)
    {
        $this->tax_slab_data = $tax_slab_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tax_slab_data  = $this->tax_slab_data;
        $id = '';
        
        if (isset($tax_slab_data['id'])) {
            $id =$tax_slab_data['id'];
        }

        $save_data = TaxSlab::firstOrNew(['id'=>$id]);
        $save_data->fill($tax_slab_data);

        $save_data->name = $tax_slab_data['name'];
        $save_data->percentage_sgst = $tax_slab_data['percentage_sgst'];
        $save_data->percentage_cgst = $tax_slab_data['percentage_cgst'];
        $save_data->percentage_igst = $tax_slab_data['percentage_igst'];
        $save_data->status = $tax_slab_data['status'];
        
        if ($tax_slab_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
