<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Appointment;

class AppointmentJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($appointment_data)
    {
        $this->appointment_data = $appointment_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $appointment_data  = $this->appointment_data;
        // dd($appointment_data);
        $id = '';
        
        if (isset($appointment_data['id'])) {
            $id =$appointment_data['id'];
        }

        $save_data = Appointment::firstOrNew(['id'=>$id]);

        $save_data->title = $appointment_data['title'];
        $save_data->start_time = $appointment_data['start_time'];
        $save_data->end_time = $appointment_data['end_time'];
        $save_data->notes = $appointment_data['notes'];

        if (isset($appointment_data['files'])) {

            $old_file = $save_data->files;
            $foldername = 'document';
            $file_data = $appointment_data['files'];
            $filename = $this->ImageUpload($old_file,$file_data,$foldername);

            $save_data->files = $filename;
        }

        $save_data->save();
        
    }

    public function ImageUpload($old_file,$file_data,$foldername){

        if (!empty($old_image))
        {
            $path = UPLOAD_PATH."/".$foldername."/".$old_file;
            // dd($path);
            if(file_exists($path))
            {
                unlink($path);
            }
        }

        if(isset($file_data))
        {    
            $i=$file_data;

            $filename = time()."_".$i->getClientOriginalName();

            $i->move(UPLOAD_PATH."/".$foldername,$filename);

        }

        return $filename;
    }
}
