<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Quotation;
use App\Models\QuotationItem;
use App\Models\Customer;
use Auth;
use Session;
use App\Http\Traits\HelperTrait;
class QuotationDataSaveJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,HelperTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($quotation_data)
    {
        $this->quotation_data = $quotation_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $quotation_data = $this->quotation_data;
        $id ='';
        if (isset($quotation_data['id'])) {
            $id = $quotation_data['id'];
        }

        $distributor_id = Auth::guard('distributor')->user()->id;

        $customer= Customer::select('full_name','email')->where('id',$quotation_data['customer'])->first()->toArray();

        $save_data = Quotation::firstOrNew(['id'=>$id]);
        $save_data->customer_id = $quotation_data['customer'];
        $save_data->distributor_id = $distributor_id;
        $save_data->billing_address = $quotation_data['billing_address'];
        $save_data->shipping_address = $quotation_data['shipping_address'];
        $save_data->billing_pincode = $quotation_data['billing_pincode'];
        $save_data->shipping_pincode = $quotation_data['shipping_pincode'];
        $save_data->total_amount = $quotation_data['total'];
        $save_data->save();

        // store quotation item data

        $cart_data = session()->get('cart');
        session()->forget('cart');
        $quotation_id = $save_data->id;
        foreach ($cart_data as $cart_key => $cart_value) {
            $save_item_data = new QuotationItem();
            $save_item_data->quotation_id = $quotation_id;
            $save_item_data->product_id = $cart_key;
            $save_item_data->quantity = $cart_value['quantity'];
            $save_item_data->payment_days = $cart_value['paymentDays'];
            $save_item_data->payment_days_price = $cart_value['payment_days_price'];
            $save_item_data->save();

        }

        if ($quotation_data['btn_name'] == 'sent') {
            // send mail to customer
            $token = \Str::random(10);

            $update_token = Quotation::where('id',$quotation_id)->update(['email_token'=>bcrypt($token)]);

            $email_data = ['name'=>$customer['full_name'],'token'=>$token,'quotation_id'=>$quotation_id]; 

            $email_view = 'distributor.emails.quotation_email';
            $subject = 'Quotation';
            $email = $customer['email'];
            $this->SendMail($email_view,$subject,$email_data,$email);
        }

    }
}
