<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Slider;
use App\Http\Traits\ImageUploadTrait;
use Auth;

class SliderJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($slider_data)
    {
        $this->slider_data = $slider_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $slider_data  = $this->slider_data;
        $id = '';
        // dd($slider_data);
        if (isset($slider_data['id'])) {
            $id =$slider_data['id'];
        }

        $save_data = Slider::firstOrNew(['id'=>$id]);
        // $save_data->fill($slider_data);

        $save_data->title = $slider_data['title'];
        $save_data->subtitle = $slider_data['subtitle'];
        $save_data->position = $slider_data['position'];
        // $save_data->order = $slider_data['order'];
        $save_data->link = $slider_data['link'];
        $save_data->enabletitle = $slider_data['enabletitle'];
        $save_data->enablesubtitle = $slider_data['enablesubtitle'];
        
        if (isset($slider_data['image'])) {

            $old_image = $save_data->image;
            $foldername = 'slider';
            $image_data = $slider_data['image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->image = $filename;
        }
        if ($slider_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
