<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Testimonial;
use App\Http\Traits\ImageUploadTrait;
use Auth;

class TestimonialJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($testimonial_data)
    {
        $this->testimonial_data = $testimonial_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $testimonial_data = $this->testimonial_data;

        $id = '';
        
        if (isset($testimonial_data['id'])) {
            $id =$testimonial_data['id'];
        }

        $save_data = Testimonial::firstOrNew(['id'=>$id]);
    
        $save_data->name = $testimonial_data['name'];
        $save_data->designation = $testimonial_data['designation'];
        $save_data->description = $testimonial_data['description'];
        if (isset($testimonial_data['image'])) {

            $old_image = $save_data->image_url;
            $foldername = 'home-page/testimonial';
            $image_data = $testimonial_data['image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->image_url = $filename;
        }
        
        if ($testimonial_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
