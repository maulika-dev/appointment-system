<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Traits\ImageUploadTrait;
use App\Models\Brand;
use Auth;

class BrandJob 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($brand_data)
    {
        $this->brand_data = $brand_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $brand_data  = $this->brand_data;
        $id = '';
        
        if (isset($brand_data['id'])) {
            $id =$brand_data['id'];
        }

        $save_data = Brand::firstOrNew(['id'=>$id]);
        $save_data->fill($brand_data);

        $save_data->name = $brand_data['name'];
        $save_data->code = $brand_data['code'];
        $save_data->status = $brand_data['status'];
        if (isset($brand_data['image'])) {

            $old_image = $save_data->image_url;
            $foldername = 'brand';
            $image_data = $brand_data['image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->image_url = $filename;
        }
        
        if ($brand_data['segment'] == 'create') {
            $save_data->created_by = Auth::user()->id;
        }
        else
        {
            $save_data->updated_by = Auth::user()->id;
        }

        // dd($save_data);
        $save_data->save();
    }
}
