<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageUploadJob 
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $oldimage;
    public $image_data;
    public $foldername;
    public function __construct($oldimage,$image_data,$foldername)
    {
        $this->oldimage    = $oldimage;
        $this->image_data   = $image_data;
        $this->foldername = $foldername;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oldimage = $this->oldimage;
        $image_data = $this->image_data;
        $foldername = $this->foldername;
        dd('dd');
        if (!empty($oldimage))
        {
            $path = UPLOAD_PATH."/upload/images/".$foldername."/".$oldimage;
            // dd($path);
            if(file_exists($path))
            {
                unlink($path);
            }
        }

        if(isset($image_data))
        {    
            $i=$image_data;

            $filename = time()."_".$i->getClientOriginalName();
            $i->move(LOCAL_UPLOAD_PATH."/upload/images".$foldername,$filename);

        }
        dd($filename);
        return $filename;

    }
}
