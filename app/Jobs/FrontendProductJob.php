<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\FrontendProduct;
use App\Http\Traits\ImageUploadTrait;

class FrontendProductJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,ImageUploadTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product_data)
    {
        $this->product_data = $product_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product_data = $this->product_data;
        // dd($product_data);
        $id = '';
        
        if (isset($product_data['id'])) {
            $id =$product_data['id'];
        }

        $save_data = FrontendProduct::firstOrNew(['id'=>$id]);
    
        $save_data->name = $product_data['name'];
        $save_data->description = $product_data['description'];
        if (isset($product_data['image'])) {

            $old_image = $save_data->image;
            $foldername = 'home-page/product';
            $image_data = $product_data['image'];
            $filename = $this->ImageUploadTrait($old_image,$image_data,$foldername);
            // dd($filename);
            $save_data->image = $filename;
        }
    
        // dd($save_data);
        $save_data->save();
    }
}
