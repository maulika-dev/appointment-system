<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomePartsDetails extends Model
{
    use HasFactory;

    protected $table = 'home_parts_details';

    protected $fillable = ['title','description','icon_image','section_setting_id','updated_by'];

    protected $timestamp = true;
}
