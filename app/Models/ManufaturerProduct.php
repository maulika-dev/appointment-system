<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManufaturerProduct extends Model
{
    use HasFactory;

    protected $table = 'manufaturer_products';

    protected $fillable = ['manufacturer_id','product_id'];

    protected $timestamp = true;

    
}





