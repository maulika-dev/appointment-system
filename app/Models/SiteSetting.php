<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    use HasFactory;

    protected $table = 'site_setting';

    protected $fillable = ['primary_logo','secondary_logo','favicon_icon','company_name','seo_title','seo_description','contact_email','contact_no_1','contact_no_2','address','google_map_iframe','google_analytics_code','facebook_url','twitter_url','instagram_url'];

    public $timestamp = true;
}
