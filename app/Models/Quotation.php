<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\QuotationItem;

class Quotation extends Model
{
    use HasFactory;

    protected $table = 'quotations';

    protected $fillable = ['customer_id','distributor_id','date','total_amount','billing_address','shipping_address','email_token','is_approved','approval_datetime','billing_pincode','shipping_pincode'];

    protected $timestamp = true;

    public function getItems(){
        return $this->hasMany('App\Models\QuotationItem','quotation_id','id');
    }

    public function getProductImage(){
        return app('App\Models\QuotationItem')->getProduct();
    }
}
