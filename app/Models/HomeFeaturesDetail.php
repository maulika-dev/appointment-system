<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeFeaturesDetail extends Model
{
    use HasFactory;

    protected $table = 'home_features_details';

    protected $fillable = ['text','section_setting_id','updated_by'];

    protected $timestamp = true;
}
