<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CmsPage extends Model
{
    use HasFactory;

  	protected $table ="cms_pages";

  	protected $fillable = ['page_title','slug','meta_title','meta_description','page_content','created_by','updated_by'];

  	public $timestamp = true;
}
