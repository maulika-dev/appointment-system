<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SectionSetting extends Model
{
    use HasFactory;

    protected $table = 'section_settings';

    // protected $fillable = ['about_us_description','business_description','section2_title','section2_description','section2_image','section3_title','section3_btn_name','section3_btn_link','section3_description','section4_title','section4_description','section4_image','section5_title','section5_description','vision_description','vision_image','mission_description','mission_image','goal_description','goal_image'];

    protected $fillable = ['about_us_description','business_description','values_box1_title','values_box2_title','values_box3_title','values_box1_description','values_box2_description','values_box3_description','values_box1_image','values_box2_image','values_box3_image','values_title','values_description','values_image','whyus_description','whyus_title','whyus_btn_name','whyus_btn_link','whyus_short_description'];

    public $timestamp = true;
}
