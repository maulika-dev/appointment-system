<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\CustomPasswordReset;

class Distributor extends Authenticatable
{
    use HasFactory,Notifiable;

    protected $table = 'distributors';

    protected $fillable = ['name','company_name','email','password','company_type','address','area','state_id','city_id','phone_1','phone_2','poc_name','poc_email','poc_phone','poc_description','margin_total_amount','status','created_by','updated_by'];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $timestamp = true;

    public function getState()
    {
    	return $this->hasOne('App\Models\State','id','state_id');
    }

    public function getCity(){
    	return $this->hasOne('App\Models\City','id','city_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomPasswordReset($token));
    }
}
