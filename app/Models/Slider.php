<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;

    protected $table = 'sliders';

    protected $fillable = ['title','subtitle','position','order','image','link','enabletitle','enablesubtitle','created_by','updated_by'];

    protected $timestamp = true;
}
