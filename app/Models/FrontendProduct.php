<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrontendProduct extends Model
{
    use HasFactory;

    protected $table = 'frontend_products';

    protected $fillable = ['name','image','description'];

    protected $timestamp = true;

}
