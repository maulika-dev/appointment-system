<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = ['name','code','image_url','created_by','updated_by'];

    protected $timestamp = true;

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    
}
