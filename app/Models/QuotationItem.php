<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class QuotationItem extends Model
{
    use HasFactory;

    protected $table = 'quotation_items';

    protected $fillable = ['product_id','manufacturer_id','quantity','payment_days','payment_days_price','distributor_margin','delivered_quantity','quotation_id'];

    protected $timestamp = true;

    public function getProduct(){
        $this->getImages();
    }
}
