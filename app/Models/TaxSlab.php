<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxSlab extends Model
{
    use HasFactory;

    protected $table = 'tax_slabs';

    protected $fillable = ['name','percentage_sgst','percentage_cgst','percentage_igst','status','created_by','updated_by'];

    protected $timestamp = true;
}
