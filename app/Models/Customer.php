<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = ['full_name','email','contact_no','address','state_id','city_id','gst_no','created_by','updated_by'];

    protected $timestamp = true;

    public function getState()
    {
    	return $this->hasOne('App\Models\State','id','state_id');
    }

    public function getCity(){
    	return $this->hasOne('App\Models\City','id','city_id');
    }

}
