<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = ['name','description','category_id','brand_id','manufacturer_id','image_url','unit','unit_price','minimum_ord_qty','credit_days','tax_percentage','hsn_code','transportation_charges','days_of_payment','days_0','days_1_15','days_16_30','days_31_45','days_46_60','days_61_75','days_76_90','days_91_120'];

    protected $timestamp = true;

    public function setImageUrlJsonAttribute($value)
    {
        $this->attributes['image_url'] = json_decode($value);
    }

    public function getCategory(){
        return $this->hasOne('App\Models\Category','id','category_id');
    }
    public function getBrand(){
        return $this->hasOne('App\Models\Brand','id','brand_id');
    }

    public function getManufaturer(){
        return $this->hasMany('App\Models\ManufaturerProduct','product_id','id');
        // return $this->belongsTo(ManufaturerProduct::class);
    }

    public function getUnit(){
        return $this->hasOne('App\Models\Unit','id','unit');
    }

    public function getImages(){
        return $this->hasMany('App\Models\ProductImage','product_id','id');
        // return $this->belongsTo(ManufaturerProduct::class);
    }


    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }


}
