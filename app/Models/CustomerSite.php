<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerSite extends Model
{
    use HasFactory;

    protected $table = 'customer_sites';

    protected $fillable = ['site_alias','customer_id','address','state_id','city_id'];

    public $timestamp = true;

    public function getState()
    {
    	return $this->hasOne('App\Models\State','id','state_id');
    }

    public function getCity(){
    	return $this->hasOne('App\Models\City','id','city_id');
    }

    public function getCustomer()
    {
    	return $this->hasOne('App\Models\Customer','id','customer_id');
    }
}
