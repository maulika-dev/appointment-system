<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $table = 'brands';

    protected $fillable = ['name','code','image_url','category','created_by','updated_by'];

    protected $timestamp = true;

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getCategory(){
        return $this->hasOne('App\Models\Category','id','category');
    }


}
