<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeIconDetail extends Model
{
    use HasFactory;

    protected $table = 'home_icon_details';

    protected $fillable = ['title','sub_title','icon_image','section_setting_id','updated_by'];

    protected $timestamp = true;
}
