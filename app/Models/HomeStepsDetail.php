<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeStepsDetail extends Model
{
    use HasFactory;
    protected $table = 'home_steps_details';

    protected $fillable = ['title','description','section_setting_id','updated_by'];

    protected $timestamp = true;
}
