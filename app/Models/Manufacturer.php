<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    use HasFactory;

    protected $table = 'manufacturers';

    protected $fillable = ['name','address','state_id','city_id','poc_name','poc_email','poc_phone_1','poc_phone_2','poc_description','created_by','updated_by'];

    protected $timestamp = true;

    public function getState()
    {
    	return $this->hasOne('App\Models\State','id','state_id');
    }

    public function getCity(){
    	return $this->hasOne('App\Models\City','id','city_id');
    }

    // public function getManufaturer(){
    //     return $this->hasMany('App\Models\Product','id','id');
    // }

    
}
