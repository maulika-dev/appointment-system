<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $segment = $this->segment(3);
        
        $rules =  [
            'title' => 'required',
        ];

        if ($segment != null) {
            $rules['start_time'] = 'required | unique:appointments,start_time,'.$this->segment(3);
            $rules['end_time'] = 'required | unique:appointments,end_time,'.$this->segment(3);
        }
        else
        {
            $rules['start_time'] = 'required | unique:appointments,start_time';
            $rules['end_time'] = 'required | unique:appointments,end_time';
        }

        return $rules;
    }
}
