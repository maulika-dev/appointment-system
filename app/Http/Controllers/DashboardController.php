<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;

class DashboardController extends Controller
{
    public function index(){

    	$all_data = Appointment::get()->toArray();
    	
    	return view('admin.dashboard',compact('all_data'));
    }
}
