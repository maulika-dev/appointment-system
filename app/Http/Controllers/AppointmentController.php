<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\AppointmentEvent;
use App\Models\Appointment;
use App\Http\Requests\AppointmentRequest;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){

            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {

                $search = $request->input('sSearch');

                $where_str .= " and (title like \"%{$search}%\""

                . ")";

            }                                            

            $columns = ['title','start_time','end_time','id'];

            $applicant_columns_count = Appointment::select($columns)
                                        ->whereRaw($where_str, $where_params)
                                        ->count();

            $applicant_list = Appointment::select($columns)
                                ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $applicant_list = $applicant_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));

            }          

            if($request->input('iSortCol_0')){

                $sql_order='';

                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){

                        $column = substr($column, 0, $index);
                    }
                    $applicant_list = $applicant_list->orderBy($column,$request->input('sSortDir_'.$i));   

                }

            } 

            $applicant_list = $applicant_list->get();

            $response['iTotalDisplayRecords'] = $applicant_columns_count;

            $response['iTotalRecords'] = $applicant_columns_count;

            $response['sEcho'] = intval($request->input('sEcho'));

            $response['aaData'] = $applicant_list->toArray();

            return $response;

        }
        return view('admin.appointment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.appointment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentRequest $request)
    {
        $data = $request->all();
        // $data = json_decode($data,true);
        event(new AppointmentEvent($data));         
        $notification = array(
            'message' => 'Appointment created successfully!',
            'alert-type' => 'success'
        );
        
        return response()->json(['success'=>true]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(AppointmentRequest $request)
    {
        $data = $request->all();

        $data = Appointment::where('id',$data['id'])
                                ->first()->toArray();
        // dd($distributor_data);
        $modal_html = view('admin.appointment.modal_html',compact('data'))->render();

        return response()->json(['success'=>true,'html'=>$modal_html],200);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Appointment::find($id);

        return view('admin.appointment.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($appointment_data);
        $data['id'] = $id;
        event(new AppointmentEvent($data));         
        return response()->json(['success'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->get('id');
        if(!is_array($id)){
            $id = array($id);
        }

        if (!empty($id)) {
            $path = UPLOAD_PATH.'/document';
            foreach ($id as $key => $value) {
                $appointment = Appointment::find($value);
                $appointment_delete = $appointment['files'];
                $path = $path.'/'.$appointment_delete;
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }
        
        $appointment = Appointment::whereIn('id',$id)->delete();
    }
}
