<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class RedirectIfNotDistributorAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$guard = 'distributor')
    {
        // dd($guard);
        if (!Auth::guard($guard)->check()) {
            return redirect('/distributor/login');
        }
        return $next($request);
    }
}
