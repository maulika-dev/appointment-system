<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\TestimonialJob;
use App\Events\TestimonialEvent;

class TestimonialListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TestimonialEvent $event)
    {
        $testimonial_data = $event->testimonial_data;

        $testimonial_data = $this->Dispatch(new TestimonialJob($testimonial_data));
    }
}
