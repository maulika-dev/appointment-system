<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\CustomerSiteEvent;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\CustomerSiteJob;

class CustomerSiteListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CustomerSiteEvent $event)
    {
        $site_data = $event->site_data;

        $site_data = $this->Dispatch(new CustomerSiteJob($site_data));
    }
}
