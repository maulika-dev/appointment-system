<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\SiteSettingEvent;
use App\Jobs\SiteSettingJob;


class SiteSettingListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SiteSettingEvent $event)
    {
        $site_data = $event->site_data;

        $site_data = $this->Dispatch(new SiteSettingJob($site_data));
    }
}
