<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\SliderEvent;
use App\Jobs\SliderJob;

class SliderListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SliderEvent $event)
    {
        $slider_data = $event->slider_data;

        $slider_data = $this->Dispatch(new SliderJob($slider_data));
    }
}
