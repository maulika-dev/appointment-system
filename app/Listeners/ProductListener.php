<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\ProductEvent;
use App\Jobs\ProductJob;

class ProductListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductEvent $event)
    {
        $product_data = $event->product_data;

        $product_data = $this->Dispatch(new ProductJob($product_data));
    }
}
