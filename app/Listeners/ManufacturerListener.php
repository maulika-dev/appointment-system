<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\ManufacturerEvent;
use App\Jobs\ManufacturerJob;

class ManufacturerListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ManufacturerEvent $event)
    {
        $manufacturer_data = $event->manufacturer_data;

        $manufacturer_data = $this->Dispatch(new ManufacturerJob($manufacturer_data));
    }
}
