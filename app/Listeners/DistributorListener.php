<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\DistributorEvent;
use App\Jobs\DistributorJob;

class DistributorListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(DistributorEvent $event)
    {
        $distributor_data = $event->distributor_data;

        $distributor_data = $this->Dispatch(new DistributorJob($distributor_data));
    }
}
