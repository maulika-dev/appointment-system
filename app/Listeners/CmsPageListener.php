<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\CmsPageEvent;
use App\Jobs\CmsPageJob;

class CmsPageListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CmsPageEvent $event)
    {
        $page_data = $event->page_data;

        $page_data = $this->Dispatch(new CmsPageJob($page_data));
    }
}
