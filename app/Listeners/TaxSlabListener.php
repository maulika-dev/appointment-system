<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Events\TaxSlabEvent;
use App\Jobs\TaxSlabJob;

class TaxSlabListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TaxSlabEvent $event)
    {
        $tax_slab_data = $event->tax_slab_data;

        $tax_slab_data = $this->Dispatch(new TaxSlabJob($tax_slab_data));
    }
}
