<?php

namespace App\Listeners;

use App\Events\AppointmentEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\AppointmentJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AppointmentListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppointmentEvent  $event
     * @return void
     */
    public function handle(AppointmentEvent $event)
    {
        $appointment_data = $event->appointment_data;

        $appointment_data = $this->Dispatch(new AppointmentJob($appointment_data));
    }
}
