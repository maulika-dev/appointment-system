## About Appointment System

An appointment system is where the user can add/edit or view his upcoming appointment.

## Steps to run system

- Create database in database system with name 'appointment_system'
- add database credential in .evn file (Example:- DB_USERNAME=root and DB_PASSWORD= root )
- run "php artisan migrate" command to create tables in database 
- run "php artisan serve"  command to run application

