<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class BrandFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Brand::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category = Category::pluck('id')->toArray();

        return [
            'name' => $this->faker->word,
            'code' => $this->faker->text($maxNbChars = 10),
            'status' => $this->faker->numberBetween(0,1),
            'created_by' => $this->faker->numberBetween(1,1),
            'category' => $this->faker->randomElement($category),
            'image_url' => $this->faker->image('public/admin/upload/images/brand',400,400, null, false),
        ];
    }
}
