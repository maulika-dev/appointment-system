<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'code' => $this->faker->word,
            'status' => $this->faker->numberBetween(0,1),
            'created_by' => $this->faker->numberBetween(1,1),
            'image_url' => $this->faker->image('public/admin/upload/images/category',640,480, null, false),
        ];
    }
}
