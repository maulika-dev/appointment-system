<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Manufacturer;
use App\Models\Unit;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category = Category::pluck('id')->toArray();
        $brand = Brand::pluck('id')->toArray();
        $manufacturer = Manufacturer::pluck('id')->toArray();
        $unit = Unit::pluck('id')->toArray();
        return [
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'category_id' => $this->faker->randomElement($category),
            'brand_id' => $this->faker->randomElement($brand),
            'manufacturer_id' => $this->faker->randomElement($manufacturer),
            'unit' => $this->faker->randomElement($unit),
            'unit_price' => $this->faker->numberBetween(100,1000),
            'minimum_ord_qty' => $this->faker->numberBetween(5,40),
            'credit_days' => $this->faker->numberBetween(2,30),
            'tax_percentage' => $this->faker->randomFloat(2,1,100),
            'hsn_code' => $this->faker->text(5),
            'transportation_charges' => $this->faker->randomFloat(2,1,100),
            'days_of_payment' => $this->faker->numberBetween(1,30),
            'created_by' => $this->faker->numberBetween(1,1),
            // 'image_url' => $this->faker->image('public/admin/upload/images/brand',400,400, null, false),
        ];

        // App\Models\ProductImage::factory()->count(100)->create()
        // 
    }
}
