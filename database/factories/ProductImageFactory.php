<?php

namespace Database\Factories;

use App\Models\ProductImage;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Product;

class ProductImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $product = Product::pluck('id')->toArray();
        $product_id = $this->faker->randomElement($product);

        $image = 'public/admin/upload/images/product/';

        return [
            'product_id' => $product_id,
            'image_name' => $this->faker->image('public/admin/upload/images/product/',600,600,'product', false),
        ];
    }
}
